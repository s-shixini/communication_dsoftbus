/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client_trans_message_service.h"

#include "client_trans_channel_manager.h"
#include "client_trans_file.h"
#include "client_trans_file_listener.h"
#include "client_trans_session_manager.h"
#include "client_trans_session_service.h"
#include "softbus_def.h"
#include "softbus_errcode.h"
#include "softbus_feature_config.h"
#include "softbus_adapter_mem.h"
#include "trans_log.h"

int CheckSendLen(int32_t channelId, int32_t channelType, unsigned int len, int32_t businessType)
{
    uint32_t dataConfig = INVALID_DATA_CONFIG;
    if (ClientGetDataConfigByChannelId(channelId, channelType, &dataConfig) != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_SDK, "get config failed.");
        return SOFTBUS_GET_CONFIG_VAL_ERR;
    }
    if (dataConfig == 0) {
        ConfigType configType = (ConfigType)GetDefaultConfigType(channelType, businessType);
        if (configType == SOFTBUS_CONFIG_TYPE_MAX) {
            TRANS_LOGE(TRANS_SDK, "Invalid channelType=%{public}d, businessType=%{public}d",
                channelType, businessType);
            return SOFTBUS_INVALID_PARAM;
        }
        if (SoftbusGetConfig(configType, (unsigned char *)&dataConfig, sizeof(dataConfig)) != SOFTBUS_OK) {
            TRANS_LOGE(TRANS_SDK, "get config failed, configType=%{public}d.", configType);
            return SOFTBUS_GET_CONFIG_VAL_ERR;
        }
    }
    TRANS_LOGI(TRANS_SDK, "channelId=%{public}d, sendDataLen=%{public}u, maxDataLen=%{public}u",
        channelId, len, dataConfig);
    if (len > dataConfig) {
        TRANS_LOGE(TRANS_SDK, "send data over limit.len=%{public}u", len);
        return SOFTBUS_TRANS_SEND_LEN_BEYOND_LIMIT;
    }

    return SOFTBUS_OK;
}

int CheckSendLenByTransGroup(TransGroup *transGroup, unsigned int len, int32_t businessType)
{
    int32_t i = 0;
    bool checkResult = false;
    for (i = 0; i < SESSION_MAX_CHANNEL_NUM; i++) {
        if (transGroup->transInfo[i].channelId >= 0) {
            int32_t ret = CheckSendLen(transGroup->transInfo[i].channelId, transGroup->transInfo[i].channelType,
                len, businessType);
            if (ret != SOFTBUS_OK) {
                TRANS_LOGE(TRANS_SDK, "check send len err: ret=%{public}d, channelId=%{public}d, channeType=%{public}d"
				, ret, transGroup->transInfo[i].channelId, transGroup->transInfo[i].channelType);
            } else {
                checkResult = true;
            }
        }
    }
    return checkResult ? SOFTBUS_OK: SOFTBUS_ERR;
}

int SendBytes(int sessionId, const void *data, unsigned int len)
{
    TRANS_LOGI(TRANS_BYTES, "sessionId=%{public}d, len=%{public}d", sessionId, len);
    if (data == NULL || len == 0) {
        TRANS_LOGW(TRANS_BYTES, "Invalid param");
        return SOFTBUS_INVALID_PARAM;
    }

    int ret = CheckPermissionState(sessionId);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_BYTES, "SendBytes no permission, ret=%{public}d", ret);
        return ret;
    }

    SessionInfo sessionInfo;
    ret = ClientGetSessionInfoBySessionId(sessionId, &sessionInfo);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_BYTES, "get sessionInfo failed, ret=%{public}d", ret);
        return ret;
    }

    int32_t businessType = sessionInfo.businessType;
    if ((businessType != BUSINESS_TYPE_BYTE) && (businessType != BUSINESS_TYPE_NOT_CARE) &&
        (sessionInfo.transGroup.transInfo[0].channelType != CHANNEL_TYPE_AUTH)) {
        TRANS_LOGE(TRANS_BYTES, "BusinessType no match, businessType=%{public}d", businessType);
        return SOFTBUS_TRANS_BUSINESS_TYPE_NOT_MATCH;
    }

    int checkRet = CheckSendLenByTransGroup(&sessionInfo.transGroup, len, BUSINESS_TYPE_BYTE);
    if (checkRet != SOFTBUS_OK) {
        return checkRet;
    }

    if (sessionInfo.isEnable != true) {
        return SOFTBUS_TRANS_SESSION_NO_ENABLE;
    }
    (void)ClientResetIdleTimeoutById(sessionId);
    return ClientTransGroupSendBytes(&sessionInfo.transGroup, data, len);
}

int SendMessage(int sessionId, const void *data, unsigned int len)
{
    TRANS_LOGI(TRANS_MSG, "sessionId=%{public}d, len=%{public}d", sessionId, len);
    if (data == NULL || len == 0) {
        TRANS_LOGW(TRANS_MSG, "Invalid param");
        return SOFTBUS_INVALID_PARAM;
    }
    int ret = CheckPermissionState(sessionId);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_MSG, "SendMessage no permission, ret=%{public}d", ret);
        return ret;
    }

    SessionInfo sessionInfo;
    ret = ClientGetSessionInfoBySessionId(sessionId, &sessionInfo);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_BYTES, "get sessionInfo failed, ret=%{public}d", ret);
        return ret;
    }

    int32_t businessType = sessionInfo.businessType;
    if ((businessType != BUSINESS_TYPE_MESSAGE) && (businessType != BUSINESS_TYPE_NOT_CARE) &&
        (sessionInfo.transGroup.transInfo[0].channelType != CHANNEL_TYPE_AUTH)) {
        TRANS_LOGE(TRANS_MSG, "BusinessType no match, businessType=%{public}d", businessType);
        return SOFTBUS_TRANS_BUSINESS_TYPE_NOT_MATCH;
    }

    int checkRet = CheckSendLenByTransGroup(&sessionInfo.transGroup, len, BUSINESS_TYPE_BYTE);
    if (checkRet != SOFTBUS_OK) {
        return checkRet;
    }

    if (sessionInfo.isEnable != true) {
        return SOFTBUS_TRANS_SESSION_NO_ENABLE;
    }
    (void)ClientResetIdleTimeoutById(sessionId);
    return ClientTransGroupSendMessage(&sessionInfo.transGroup, data, len);
}

static int CheckTransGroup(TransGroup *transGroup, ChannelType channelType)
{
    int32_t i = 0;
    for (i = 0; i < SESSION_MAX_CHANNEL_NUM; i++) {
        if (transGroup->transInfo[i].channelId >= 0) {
            if (channelType != (ChannelType) transGroup->transInfo[i].channelType) {
                TRANS_LOGE(TRANS_SDK, "trans group isn't require type, channeType=%{public}d",
                    transGroup->transInfo[i].channelType);
                return SOFTBUS_ERR;
            }
        }
    }
    return SOFTBUS_OK;
}

int SendStream(int sessionId, const StreamData *data, const StreamData *ext, const StreamFrameInfo *param)
{
    if ((data == NULL) || (ext == NULL) || (param == NULL)) {
        TRANS_LOGW(TRANS_STREAM, "Invalid param");
        return SOFTBUS_INVALID_PARAM;
    }
    int32_t ret = CheckPermissionState(sessionId);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_STREAM, "SendStream no permission, ret=%{public}d", ret);
        return ret;
    }

    SessionInfo sessionInfo;
    ret = ClientGetSessionInfoBySessionId(sessionId, &sessionInfo);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_STREAM, "get sessionInfo failed, ret=%{public}d", ret);
        return ret;
    }
    if (CheckTransGroup(&sessionInfo.transGroup, CHANNEL_TYPE_UDP)) {
        return SOFTBUS_TRANS_STREAM_ONLY_UDP_CHANNEL;
    }

    int32_t businessType = sessionInfo.businessType;
    if ((businessType != BUSINESS_TYPE_STREAM) && (businessType != BUSINESS_TYPE_NOT_CARE)) {
        TRANS_LOGE(TRANS_STREAM, "BusinessType no match, businessType=%{public}d", businessType);
        return SOFTBUS_TRANS_BUSINESS_TYPE_NOT_MATCH;
    }

    if (sessionInfo.isEnable != true) {
        return SOFTBUS_TRANS_SESSION_NO_ENABLE;
    }
    (void)ClientResetIdleTimeoutById(sessionId);
    return ClientTransGroupSendStream(&sessionInfo.transGroup, data, ext, param);
}

int SendFile(int sessionId, const char *sFileList[], const char *dFileList[], uint32_t fileCnt)
{
    TRANS_LOGI(TRANS_FILE, "sessionId=%{public}d", sessionId);
    if ((sFileList == NULL) || (fileCnt == 0)) {
        TRANS_LOGW(TRANS_FILE, "Invalid param");
        return SOFTBUS_INVALID_PARAM;
    }
    int ret = CheckPermissionState(sessionId);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_FILE, "SendFile no permission, ret=%{public}d", ret);
        return ret;
    }

    FileSchemaListener *fileSchemaListener = (FileSchemaListener*)SoftBusCalloc(sizeof(FileSchemaListener));
    if (fileSchemaListener == NULL) {
        return SOFTBUS_MALLOC_ERR;
    }
    if (CheckFileSchema(sessionId, fileSchemaListener) == SOFTBUS_OK) {
        if (SetSchemaCallback(fileSchemaListener->schema, sFileList, fileCnt) != SOFTBUS_OK) {
            TRANS_LOGE(TRANS_FILE, "set schema callback failed");
            SoftBusFree(fileSchemaListener);
            return SOFTBUS_ERR;
        }
    }

    SessionInfo sessionInfo;
    ret = ClientGetSessionInfoBySessionId(sessionId, &sessionInfo);
    if (ret != SOFTBUS_OK) {
        TRANS_LOGE(TRANS_FILE, "get sessionInfo failed, ret=%{public}d", ret);
        return ret;
    }

    int32_t channelId = sessionInfo.transGroup.transInfo[0].channelId;
    int32_t channelType = sessionInfo.transGroup.transInfo[0].channelType;
    int32_t businessType = sessionInfo.businessType;
    if ((businessType != BUSINESS_TYPE_FILE) && (businessType != BUSINESS_TYPE_NOT_CARE)) {
        TRANS_LOGE(TRANS_FILE, "BusinessType no match, businessType=%{public}d", businessType);
        SoftBusFree(fileSchemaListener);
        return SOFTBUS_TRANS_BUSINESS_TYPE_NOT_MATCH;
    }

    if (sessionInfo.isEnable != true) {
        SoftBusFree(fileSchemaListener);
        return SOFTBUS_TRANS_SESSION_NO_ENABLE;
    }
    SoftBusFree(fileSchemaListener);
    (void)ClientResetIdleTimeoutById(sessionId);
    return ClientTransChannelSendFile(channelId, channelType, sFileList, dFileList, fileCnt);
}
